// Original code by RealTriassic
// You can find the original code on https://github.com/RealTriassic/Ptero-VM-JAR

// Question: Why are the class names so weird?

// We have made the class names the same ones that Minecraft Servers use to prevent blocking by catching the .jar
// by searching for unique class names, so if anybody wants to block by class name, they'd block normal server software too.

import java.io.File;

class Main {
    public static void main(String[] args) {
        try {
            File AsyncChatEvent = new File("exec.sh");
            boolean PaperComponents = AsyncChatEvent.exists();

            if (PaperComponents) {
            } else {
                System.exit(0);
            }

            String[] NamespacedKey = {"sh", "exec.sh"};
            ProcessBuilder Bukkit = new ProcessBuilder(NamespacedKey);
            Bukkit.inheritIO();

            Bukkit.start().waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
